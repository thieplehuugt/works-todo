<?php

namespace Tests;

use \PHPUnit\Framework\TestCase;
use Faker\Factory as Faker;
use App\Models\Work;

class WorkTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();        
    }

    /**
     * Test function get list works for index page
     *
     * @return void
     */
    public function testList(){
        $faker = Faker::create();
        $work = new Work();
        $work->work_name = $faker->name;
        $work->start_date = $faker->date('Y/m/d');
        $work->end_date = $faker->date('Y/m/d');
        $work->status = rand(1,3);

        $work->save();
        $works = Work::all();
        $this->assertIsArray($works);
        $first = $works[0];
        $this->assertInstanceOf(Work::class,$first);
    }

    /**
     * Test function store work for create page
     *
     * @return void
     */
    public function testStore(){

        $faker = Faker::create();
        $work = new Work();
        $work->work_name = $faker->name;
        $work->start_date = $faker->date('Y/m/d');
        $work->end_date = $faker->date('Y/m/d');
        $work->status = rand(1,3);

        $workCreated = $work->save();
        $this->assertGreaterThan(0, $work->id);
        $this->assertEquals($workCreated->work_name, $work->work_name);
        $this->assertEquals($workCreated->start_date, $work->start_date);
        $this->assertEquals($workCreated->end_date, $work->end_date);
        $this->assertEquals($workCreated->status, $work->status);
    }

    /**
     * Test function edit work for edit page
     *
     * @return void
     */
    public function testShow(){
        $faker = Faker::create();
        $work = new Work();
        $work->work_name = $faker->name;
        $work->start_date = $faker->date('Y/m/d');
        $work->end_date = $faker->date('Y/m/d');
        $work->status = rand(1,3);

        $workCreated = $work->save();
        $workShow = Work::find($workCreated->id);
        $this->assertGreaterThan(0, $workShow->id);
        $this->assertEquals($workShow->id, $workCreated->id);
        $this->assertEquals($workShow->work_name, $workCreated->work_name);
        $this->assertEquals($workShow->start_date, $workCreated->start_date);
        $this->assertEquals($workShow->end_date, $workCreated->end_date);
        $this->assertEquals($workShow->status, $workCreated->status);
    }

    /**
     * Test function update work for update page
     *
     * @return void
     */
    public function testUpdate(){
        $faker = Faker::create();
        $work = new Work();
        $work->work_name = $faker->name;
        $work->start_date = $faker->date('Y/m/d');
        $work->end_date = $faker->date('Y/m/d');
        $work->status = rand(1,3);

        $workCreated = $work->save();
        $workCreated = Work::find($workCreated->id);
        $fakerUpdate = Faker::create();
        $workCreated->work_name = $fakerUpdate->name;
        $workCreated->start_date = $fakerUpdate->date('Y/m/d');
        $workCreated->end_date = $fakerUpdate->date('Y/m/d');
        $workCreated->status = rand(1,3);
        $workCreated->save();
        $workUpdated = Work::find($workCreated->id);
        $this->assertGreaterThan(0, $workUpdated->id);
        $this->assertEquals($workUpdated->id, $workCreated->id);
        $this->assertEquals($workUpdated->work_name, $workCreated->work_name);
        $this->assertEquals($workUpdated->start_date, $workCreated->start_date);
        $this->assertEquals($workUpdated->end_date, $workCreated->end_date);
        $this->assertEquals($workUpdated->status, $workCreated->status);
    }
    
    /**
     * Test function destroy work for delete page
     *
     * @return void
     */
    public function testDestroy(){
        $faker = Faker::create();
        $work = new Work();
        $work->work_name = $faker->name;
        $work->start_date = $faker->date('Y/m/d');
        $work->end_date = $faker->date('Y/m/d');
        $work->status = rand(1,3);

        $work = $work->save();
        $workDelete = Work::find($work->id);
        $workDelete->delete();

        // recheck work exits in database
        $workDeleted = Work::find($work->id);
        $this->assertNull($workDeleted);

    }
    
    protected function tearDown(): void
    {
        parent::tearDown();
    }
}