CREATE TABLE IF NOT EXISTS works (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    work_name VARCHAR(255) NOT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    status int NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    modified_at DATETIME DEFAULT CURRENT_TIMESTAMP
)