<?php

namespace Core;

use PDO;
use PDOException;
use \Core\Config;

abstract class Model
{
    /**
     * Get the PDO database connection
     *
     * @return db
     */
    protected static function getDB()
    {
        static $db = null;

        $database = Config::load('database');
        if ($db === null) {
            try {
                $dsn = 'mysql:host=' . $database['connections']['mysql']['host'] . ';dbname=' . $database['connections']['mysql']['database'] . ';charset=utf8';
                $db = new PDO($dsn, $database['connections']['mysql']['username'], $database['connections']['mysql']['password']);

                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
                exit;
            }
        }

        return $db;
    }
}
