<?php

namespace Core;

class Config
{    
    /**
     * Load config file
     * 
     * @param   string
     * @desc
     */
    public static function load($config)
    {
        $arr = array();
        if (file_exists(dirname(__DIR__)  . '/config/' . $config . '.php')){
            $config = include_once dirname(__DIR__)  . '/config/' . $config . '.php';
            if ( !empty($config) && is_array($config)){
                foreach ($config as $key => $item){
                    $arr[$key] = $item;
                }
            }
            return $arr;
        }
        return $arr;
    }
}