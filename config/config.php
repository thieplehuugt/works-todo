<?php

return [
    'status' => [
        1 => 'Planning',
        2 => 'Doing',
        3 => 'Done',
    ]
];