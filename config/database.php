<?php

return [
    'connections' => [

        'mysql' => [
            'driver' => 'mysql',
            'url' => '127.0.0.1',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'work-todo',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
        ],

    ],
];