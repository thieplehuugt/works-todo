<?php

namespace App\Models;

use PDO;
use PDOException;
use \Core\Model;
class Work extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'works';
    
    /**
     * The primary key for the model.
     *
     * @var int
     */
    public $id;

    /**
     * The atribute work_name.
     *
     * @var string
     */
    public $work_name;

    /**
     * The atribute start_date.
     *
     * @var string
     */
    public $start_date;

    /**
     * The atribute end_date.
     *
     * @var string
     */
    public $end_date;

    /**
     * The atribute status.
     *
     * @var int
     */
    public $status;

    /**
     * Get all groups row
     *
     * @var array groups
     */
    public static function all(){
        try {
            $db = Model::getDB();
            $query = "SELECT * FROM works ORDER BY modified_at DESC";
            $rows = $db->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $works = [];
            foreach($rows as $row){
                $work = new Work();
                $work->id = $row['id'];
                $work->work_name = $row['work_name'];
                $work->start_date = (new \DateTime($row['start_date']))->format('Y/m/d');
                $work->end_date = (new \DateTime($row['end_date']))->format('Y/m/d');
                $work->status = $row['status'];
                $works[] = $work;
           }
           return $works;
        } catch (PDOException $e) {
            echo 'Query Error: ' . $e->getMessage();
            exit;
        }
    }

    /**
     * Find group by id
     *
     * @var object group
     */
    public static function find($id){
        try {
            $db = Model::getDB();
            $query = "SELECT * FROM works WHERE id = $id LIMIT 1";
            $rows = $db->query($query)->fetchAll(PDO::FETCH_ASSOC);
            if(count($rows) > 0){
                $work = new Work();
                $work->id = $rows[0]['id'];
                $work->work_name = $rows[0]['work_name'];
                $work->start_date = (new \DateTime($rows[0]['start_date']))->format('Y/m/d');
                $work->end_date = (new \DateTime($rows[0]['end_date']))->format('Y/m/d');
                $work->status = $rows[0]['status'];
                return $work;
            }
        } catch (PDOException $e) {
            echo 'Query Error: ' . $e->getMessage();
            exit;
        }
        return null;
    }

    /**
     * Add/update group into database
     *
     * @var object group
     */
    public function save(){
        try {
            $db = Model::getDB();
        
            if($this->id){
                $this->modified_at = date('Y-m-d H:i:s');
                $query = "UPDATE $this->table
                SET work_name=\"$this->work_name\", start_date=\"$this->start_date\", end_date=\"$this->end_date\",status=$this->status, modified_at=\"$this->modified_at\" 
                WHERE id=$this->id";        
                $db->query($query);
                $id = $this->id;
            }else{
                $query = "INSERT INTO $this->table (work_name, start_date, end_date, status)
                VALUES (\"$this->work_name\", \"$this->start_date\", \"$this->end_date\", $this->status);";        
                $db->query($query);
                $id = $db->lastInsertId();
                $this->id = $id;
            }
        } catch (PDOException $e) {
            echo 'Query Error: ' . $e->getMessage();
            exit;
        }
        return $this;
    }

    /**
     * Delete group buy id
     *
     * @var object group
     */
    public function delete(){
        try {
            $db = Model::getDB();
        
            $query = "DELETE FROM $this->table WHERE id=$this->id;";        
            $db->query($query);
        } catch (PDOException $e) {
            echo 'Query Error: ' . $e->getMessage();
            exit;
        }
    }
}
