<?php

namespace App\Controllers;

use App\Models\Work;
use \Core\View;
use \Core\Config;

class WorkController extends \Core\Controller
{
    /**
     * validate form
     *
     * @return void
     */
    public function validate($input)
    {
        $errors = [];
        if(!isset($input['work_name']) || empty($input['work_name'])){
            $errors[] = "Work Name is required";
        }
        if(!isset($input['start_date']) || empty($input['start_date'])){
            $errors[] = "Start Date is required";
        }
        if(!isset($input['end_date']) || empty($input['end_date'])){
            $errors[] = "End Date is required";
        }
        if(!isset($input['status']) || empty($input['status'])){
            $errors[] = "Status is required";
        }
        return $errors;
    }

    /**
     * Show the index page
     *
     * @return void
     */
    public function index()
    {
        $rows = Work::all();$config = Config::load('config');
        $listStatus = $config['status'];
        $notification = null;
        session_start();
        if(isset($_SESSION['notification'])){
            $notification = $_SESSION['notification'];
            unset($_SESSION['notification']);
        }
        View::render('Work/index', ['works'=>$rows, 'listStatus' => $listStatus, 'notification' => $notification]);
    }

    /**
     * Show the create page
     *
     * @return void
     */
    public function create()
    {
        $config = Config::load('config');
        $listStatus = $config['status'];       
        View::render('Work/create', ['listStatus' => $listStatus]);
    }

    /**
     * store work
     *
     * @return void
     */
    public function store()
    {
        $input = $_POST;
        $errors = $this->validate($input);
        if(count($errors) > 0){
            View::render('Work/create', ['errors' => $errors]);
            return;
        }
        $work = new Work();
        $work->work_name = $input['work_name'];
        $work->start_date = $input['start_date'];
        $work->end_date = $input['end_date'];
        $work->status = $input['status'];
        $work = $work->save();
        if($work){
            session_start();
            $_SESSION['notification'] = "Create Work success";
            $url = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
            header('Location: ' . $url, true);
            die();
        }
    }

    /**
     * edit page
     *
     * @return void
     */
    public function edit()
    {
        $query = $_GET;
        $id = $query['id'];
        $work = Work::find($id);
        $config = Config::load('config');
        $listStatus = $config['status'];
        View::render('Work/edit', ['work' => $work, 'listStatus' => $listStatus]);
    }

    /**
     * update work
     *
     * @return void
     */
    public function update()
    {
        $input = $_POST;        
        $work = Work::find($input['id']);
        $work->work_name = $input['work_name'];
        $work->start_date = $input['start_date'];
        $work->end_date = $input['end_date'];
        $work->status = $input['status'];

        $errors = $this->validate($input);
        if(count($errors) > 0){
            View::render('Work/edit', ['errors' => $errors, 'work' => $work]);
            return;
        }

        $id = $work->save();
        if($id){
            session_start();
            $_SESSION['notification'] = "Edit Work success";
            $url = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
            header('Location: ' . $url, true);
            die();
        }
    }

    /**
     * destroy work
     *
     * @return void
     */
    public function destroy()
    {
        $query = $_GET;
        $id = $query['id'];
        $work = Work::find($id);
        if($work){
            $work->delete();
        }
        if($id){
            $url = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
            header('Location: ' . $url, true);
            die();
        }
    }
}
