<div class = "row mt-2">
    <?php
        if(isset($errors)){
            foreach($errors as $error){
                echo '<p class = "validate-error">'.$error.'</p>';
            }
        }
    ?>
</div>
<div class = "row mt-2 mb-2">
    <form action="<?= isset($work) ? "update?id=" . $work->id : "store"; ?>" method = "POST">
        <?php if(isset($work)):?>
        <input type= "hidden" name = "id" value = "<?= $work->id; ?>">
        <?php endif; ?>
        <label for="work-name">Work name:</label>
        <input type="text" id="work-name" name="work_name" value = "<?= isset($work) ? $work->work_name : ''; ?>" autocomplete="off"><br><br>
        <label for="start-date">Start Date:</label>
        <input type="text" id="start-date" class = "date-picker" name="start_date" value = "<?= isset($work) ? (new DateTime($work->start_date))->format('Y/m/d') : ''; ?>" autocomplete="off"><br><br>
        <label for="end-date">End Date:</label>
        <input type="text" id="end-date" class = "date-picker"  name="end_date" value = "<?= isset($work) ? (new DateTime($work->end_date))->format('Y/m/d')  : ''; ?>" autocomplete="off"><br><br>
        <label for="status">Status:</label>
        <select id="status" name="status">
            <?php foreach($listStatus as $status){ 
                $key = array_search ($status, $listStatus);
            ?>

                <option value = "<?= $key; ?>" <?= isset($work) && $work->status == $key ? 'selected': ''; ?>><?= $status; ?></option>
            <?php } ?>
        </select><br><br>
        <div class = "right-button"><button type="submit" class = "btn btn-primary" value="Submit">Save</button></div>
    </form>
</div>
