<?php
include 'components/header.php';

?>

<div class = "row mt-2">
    <a href = "create" class = "btn btn-primary">Create Work</a>
</div>
<?php if($notification != null) :?>
<div class = "row mt-2">
    <p class = "notification"><?= $notification;?></p>
</div>
<?php endif; ?>
<div class = "row mt-2 mb-2">
    <table>
        <thead>
            <tr>
                <th>Work Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($works as $work): ?>
                <tr>
                    <td><?= $work->work_name; ?></td>
                    <td><?= (new DateTime($work->start_date))->format('Y/m/d'); ?></td>
                    <td><?= (new DateTime($work->end_date))->format('Y/m/d'); ?></td>
                    <td><?= $listStatus[$work->status]; ?></td>
                    <td><a href = "edit?id=<?= $work->id; ?>">Edit</a> | <a href = "delete?id=<?= $work->id; ?>" id = "delete">Delete</a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
include 'components/footer.php';
?>