<?php
include 'components/header.php';
?>

<div class = "row mt-2">
    <a href = "#" class = "btn btn-primary" id = "btn_back">Back</a>
</div>

<?php include 'form.php'; ?>

<?php
include 'components/footer.php';
?>