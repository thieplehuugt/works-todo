<?php

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);

require_once dirname(__DIR__) .'/router/web.php';
    
$router->dispatch($_SERVER['QUERY_STRING']);
