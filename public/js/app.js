$( document ).ready(function() {
    $('#delete').on('click', function() {
        var r = confirm("Do you want delete work?");
        if (r == false) {
            return false;
        }
    });
    
    $( ".date-picker" ).datepicker({ dateFormat: 'yy/mm/dd' });
    $("#btn_back").click(function(event) {
        event.preventDefault();
        history.back(1);
    });
});