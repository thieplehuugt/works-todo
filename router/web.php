<?php

$router = new Core\Router();

$router->add('', ['controller' => 'WorkController', 'action' => 'index']);
$router->add('create', ['controller' => 'WorkController', 'action' => 'create']);
$router->add('store', ['controller' => 'WorkController', 'action' => 'store']);
$router->add('edit', ['controller' => 'WorkController', 'action' => 'edit']);
$router->add('update', ['controller' => 'WorkController', 'action' => 'update']);
$router->add('delete', ['controller' => 'WorkController', 'action' => 'destroy']);